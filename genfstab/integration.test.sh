dir_mount=$(mktemp --directory);
trap "rm -rf $dir_mount" EXIT;

mounts="$(cat <<-HERE
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
sys /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
run /run tmpfs rw,nosuid,nodev,noexec,relatime,mode=233 0 0
dev /dev devtmpfs rw,nosuid,relatime,size=4281916k,nr_inodes=1042901,mode=233 0 0
efivarfs /sys/firmware/efi/efivars efivarfs rw,nodev,relatime 0 0
securityfs /sys/kernel/security securityfs rw,nosuid,nodev,noexec,relatime 0 0
tmpfs /dev/shm tmpfs rw,nosuid,nodev 0 0
devpts /dev/pts devpts rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
tmpfs /sys/fs/cgroup tmpfs ro,nosuid,nodev,noexec,mode=755 0 0
/dev/sdx5 / ext4 defaults 0 0
cgroup2 /sys/fs/cgroup/unified cgroup2 rw,nosuid,nodev,noexec,relatime,nsdelegate 0 0
pstore /sys/fs/pstore pstore rw,nosuid,nodev,noexec,relatime 0 0
bpf /sys/fs/bpf bpf rw,nosuid,nodev,noexec,relatime,mode=700 0 0
cgroup /sys/fs/cgroup/freezer cgroup rw,nosuid,nodev,noexec,relatime,freezer 0 0
cgroup /sys/fs/cgroup/net_cls,net_prio cgroup rw,nosuid,nodev,noexec,relatime,net_cls,net_prio 0 0
cgroup /sys/fs/cgroup/cpu,cpuacct cgroup rw,nosuid,nodev,noexec,relatime,cpu,cpuacct 0 0
cgroup /sys/fs/cgroup/pids cgroup rw,nosuid,nodev,noexec,relatime,pids 0 0
cgroup /sys/fs/cgroup/memory cgroup rw,nosuid,nodev,noexec,relatime,memory 0 0
cgroup /sys/fs/cgroup/rdma cgroup rw,nosuid,nodev,noexec,relatime,rdma 0 0
cgroup /sys/fs/cgroup/devices cgroup rw,nosuid,nodev,noexec,relatime,devices 0 0
cgroup /sys/fs/cgroup/blkio cgroup rw,nosuid,nodev,noexec,relatime,blkio 0 0
cgroup /sys/fs/cgroup/perf_event cgroup rw,nosuid,nodev,noexec,relatime,perf_event 0 0
cgroup /sys/fs/cgroup/cpuset cgroup rw,nosuid,nodev,noexec,relatime,cpuset 0 0
cgroup /sys/fs/cgroup/hugetlb cgroup rw,nosuid,nodev,noexec,relatime,hugetlb 0 0
mqueue /dev/mqueue mqueue rw,relatime 0 0
tmpfs /tmp tmpfs rw,nosuid,nodev 0 0
/dev/sdx6 /var ext4 defaults 0 0
/dev/sdx7 /mountain ext3 defaults 0 0
/dev/sdu1 $dir_mount ext4 defaults 0 0
/dev/sdu4 $dir_mount/mountain ext3 defaults 0 0
/dev/sdu5 $dir_mount/home ext2 defaults 0 0
HERE
)";

swaps="$(cat <<-HERE
Filename                                Type            Size    Used    Priority
/dev/sdu2                               partition       8388604 42189   -6
HERE
)";

cat() {
	case $1 in
		/proc/mounts)
			echo "$mounts";
			return;
			;;
		/proc/swaps)
			echo "$swaps";
			return;
			;;
	esac
	echo "cat $@" 1>&2;
	return 1;
}

lsblk() {
	case $5 in
		/dev/sdu*)
			case $4 in
				MOUNTPOINT)
					echo "$mounts" | awk '{ if ($1=="'$5'") { print $2; exit } }';
					return;
					;;
				UUID)
					echo "aaaaaaaa-$5-cccc-ffff-ggggggggg"
					return;
					;;
				FSTYPE)
					echo "$mounts" | awk '{ if ($1=="'$5'") { print $3; exit } }';
					return;
					;;
			esac
			;;
	esac
	echo "lsblk $@" 1>&2;
	return 1;
}

ws() {
	case $1 in
		--config)
			case $2 in
				dir/mount)
					echo "$dir_mount";
					return;
					;;
			esac
			;;
	esac
	echo "ws $@" 1>&2;
	return 1;
}

. "genfstab/RUN"
! ! assert_equals "$(command cat "$dir_mount/etc/fstab")" "$(command cat <<-HERE
# Automatically generated file. Do not edit!

# /dev/sdu1
UUID=aaaaaaaa-/dev/sdu1-cccc-ffff-ggggggggg	/	ext4	defaults	0 1

# /dev/sdu5
UUID=aaaaaaaa-/dev/sdu5-cccc-ffff-ggggggggg	/home	ext2	defaults	0 2

# /dev/sdu4
UUID=aaaaaaaa-/dev/sdu4-cccc-ffff-ggggggggg	/mountain	ext3	defaults	0 2

# /dev/sdu2
UUID=aaaaaaaa-/dev/sdu2-cccc-ffff-ggggggggg	none	swap	defaults	0 0
HERE
)" || exit 1
