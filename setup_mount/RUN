#!/usr/bin/env bash

device="$(ws --config device)" || exit 1;

mounts="$(ws --config disk/mount)" || exit 1;
types="$(ws --config disk/fs)" || exit 1;

partitions=$(
	(
		echo "$mounts" | awk '{ print $0":"2 }'
		echo "$types" | awk '{ print $0":"3 }'
	) | awk -F: "$(cat <<-AWK
	BEGIN { count=1; }
	{
		col=\$NF;
		id=\$1;
		val=\$2;

		cols[id":"col] = val;

		if (!counts[id]) { counts[id] = count++ }
		lines[counts[id]] = id":"cols[id":"2]":"cols[id":"3];
	}
	END {
		for (i = 1; i < count; i++) {
			print(lines[i]);
		}
	}
	AWK
	)"
);

# Order partitions by the length of their mountpoint.
# This ensures that / is mounted before /home, /home is mounted before /home/carl, etc...
partitions=$(
	echo "$partitions" | awk -F: '{ print length($2)":"$0 }' | sort -n | awk '{ sub(/^[^:]*:/, "", $0); print($0); }'
);

# Disallow (implicit) mounting to /.
root_directory="$(ws --config dir/mount)" || exit 1;

for part in $partitions; do
	number=$(echo "$part" | awk -F: '{ print $1 }');
	mountpoint=$(echo "$part" | awk -F: '{ print $2 }');
	type=$(echo "$part" | awk -F: '{ print $3 }');

	# Not all types mount the same.
	# For example, LUKS partitions need to be found by their label, and sometimes
	# decrypted.
	# Swapspace needs to be "swapon" instead of mounted.
	block="$(ws device_name "device=$device" "partition=$number")" || exit 1;

	case "$type" in
		swap)
			log "Enabling swap space $block";
			swapon "$block";
			;;
		luks*)
			lukslabel="luks-$(basename "$block")";
			ws -do unlock "device=$block" "label=$lukslabel" || exit 1;
			# TODO: This doesn't really support LUKS in LUKS... but now to think of it
			# neither does my head. Why would you even do that
			[ -n "$mountpoint" ] || continue;
			ws -do mount "device=/dev/mapper/$lukslabel" "mountpoint=$root_directory/$mountpoint" || exit 1;
			;;
		*)
			[ -n "$mountpoint" ] || continue;
			ws -do mount "device=$block" "mountpoint=$root_directory/$mountpoint" || exit 1;
			;;
	esac
done;
