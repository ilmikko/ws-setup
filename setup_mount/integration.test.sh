log() {
	echo $@ 1>&2;
}

swapon() {
	echo "swapon $@";
}

ws() {
	case $1 in
		-check|-do|-run)
			case $2 in
				mount|unlock)
					echo "$@";
					return;
					;;
			esac
			;;
		--config)
			case $2 in
				device)
					echo "$device";
					return;
					;;
				disk/mount)
					echo "$disk_mount";
					return;
					;;
				disk/fs)
					echo "$disk_fs";
					return;
					;;
				dir/mount)
					echo "$dir_mount";
					return;
					;;
			esac
			;;
		device_name)
			command ws "$@";
			return;
			;;
	esac
	echo $@ 1>&2;
}

device="/dev/sdu";
dir_mount="/mount";

disk_mount="$(cat <<-HERE
1:/boot
4:/home
2:swap
6:/mountain
7:/
HERE
)";

disk_fs="$(cat <<-HERE
1:vfat
2:swap
4:luks
7:ext3
HERE
)";

test_mount_check() {
	! ! assert_equals "$(. "setup_mount/CHECK")" "$(cat <<-HERE
	-check mount device=/dev/sdu1 mountpoint=/mount//boot
	-check unlock device=/dev/sdu4 label=luks-sdu4
	-check mount device=/dev/mapper/luks-sdu4 mountpoint=/mount//home
	-check mount device=/dev/sdu6 mountpoint=/mount//mountain
	-check mount device=/dev/sdu7 mountpoint=/mount//
	HERE
	)" || exit 1;
}

test_mount_run() {
	disk_mount="$(cat <<-HERE
	1:/
	5:/boot
	4:/home
	2:swap
	6:/mountain
	HERE
	)";

	! ! assert_equals "$(. "setup_mount/RUN")" "$(cat <<-HERE
	-do mount device=/dev/sdu1 mountpoint=/mount//
	swapon /dev/sdu2
	-do unlock device=/dev/sdu4 label=luks-sdu4
	-do mount device=/dev/mapper/luks-sdu4 mountpoint=/mount//home
	-do mount device=/dev/sdu5 mountpoint=/mount//boot
	-do mount device=/dev/sdu6 mountpoint=/mount//mountain
	HERE
	)" || exit 1;
}

test_mount_run_order() {
	disk_mount="$(cat <<-HERE
	5:/home/asd
	4:/root
	2:swap
	8:/
	6:/root/should/be/mounted/first
	HERE
	)";

	! ! assert_equals "$(. "setup_mount/RUN")" "$(cat <<-HERE
	-do mount device=/dev/sdu8 mountpoint=/mount//
	swapon /dev/sdu2
	-do unlock device=/dev/sdu4 label=luks-sdu4
	-do mount device=/dev/mapper/luks-sdu4 mountpoint=/mount//root
	-do mount device=/dev/sdu5 mountpoint=/mount//home/asd
	-do mount device=/dev/sdu6 mountpoint=/mount//root/should/be/mounted/first
	HERE
	)" || exit 1;
}
