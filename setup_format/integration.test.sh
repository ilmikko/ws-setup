ws() {
	echo $@ 1>&2;
	case $1 in
		-check|-do|-run)
			case $2 in
				format)
					echo "$@";
					;;
			esac
			;;
		--config)
			case $2 in
				device)
					echo "$device";
					;;
				disk/label)
					echo "$disk_label";
					;;
				disk/fs)
					echo "$disk_fs";
					;;
			esac
			;;
		device_name)
			command ws "$@";
			;;
	esac
}

device="/dev/sdu";

disk_label="$(cat <<-HERE
1:boot
4:home
6:notused
7:root
HERE
)";

disk_fs="$(cat <<-HERE
1:vfat
2:fat32
4:ext4
7:ext3
HERE
)";

test_check_generic() {
	! ! assert_equals "$(. "setup_format/CHECK")" "$(cat <<-HERE
	-check format device=/dev/sdu1 type=vfat label=boot
	-check format device=/dev/sdu2 type=fat32 label=
	-check format device=/dev/sdu4 type=ext4 label=home
	-check format device=/dev/sdu7 type=ext3 label=root
	HERE
	)" || exit 1;
}

test_check_fs() {
	disk_fs="$(cat <<-HERE
	2:fat32
	HERE
	)";

	! ! assert_equals "$(. "setup_format/CHECK")" "$(cat <<-HERE
	-check format device=/dev/sdu2 type=fat32 label=
	HERE
	)" || exit 1;
}

test_check_label() {
	disk_label="$(cat <<-HERE
	2:label
	2:otherlabel
	HERE
	)";

	disk_fs="$(cat <<-HERE
	2:fat32
	HERE
	)";

	! ! assert_equals "$(. "setup_format/CHECK")" "$(cat <<-HERE
	-check format device=/dev/sdu2 type=fat32 label=otherlabel
	HERE
	)" || exit 1;
}

test_run() {
	! ! assert_equals "$(. "setup_format/RUN")" "$(cat <<-HERE
	-do format device=/dev/sdu1 type=vfat label=boot
	-do format device=/dev/sdu2 type=fat32 label=
	-do format device=/dev/sdu4 type=ext4 label=home
	-do format device=/dev/sdu7 type=ext3 label=root
	HERE
	)" || exit 1;
}
