log() {
	echo $@;
}

device="/dev/sdu";
fdisk_file="$(mktemp)";
trap "rm $fdisk_file" EXIT;

fdisk() {
	case "$1" in
		-l|--list)
			case "$2" in
				/dev/sdu1)
					cat <<-HERE
					Disk /dev/sdu1: 20 GB, 20000000000 bytes, 71943040 sectors
					Units: sectors of 1 * 1024 = 1024 bytes
					Sector size (logical/physical): 1024 bytes / 512 bytes
					I/O size (minimum/optimal): 1024 bytes / 512 bytes
					HERE
					;;
				/dev/sdu2)
					cat <<-HERE
					Disk /dev/sdu2: 4 GiB, 4000000000 bytes, 71943040 sectors
					Units: sectors of 1 * 1024 = 1024 bytes
					Sector size (logical/physical): 1024 bytes / 512 bytes
					I/O size (minimum/optimal): 1024 bytes / 512 bytes
					HERE
					;;
				/dev/sdu3)
					cat <<-HERE
					Disk /dev/sdu3: 100 MiB, 100000000 bytes, 71943040 sectors
					Units: sectors of 1 * 1024 = 1024 bytes
					Sector size (logical/physical): 1024 bytes / 512 bytes
					I/O size (minimum/optimal): 1024 bytes / 512 bytes
					HERE
					;;
				/dev/sdu4)
					cat <<-HERE
					Disk /dev/sdu4: 90 GB, 90000000000 bytes, 71943040 sectors
					Units: sectors of 1 * 1024 = 1024 bytes
					Sector size (logical/physical): 1024 bytes / 512 bytes
					I/O size (minimum/optimal): 1024 bytes / 512 bytes
					HERE
					;;
				/dev/sdu5)
					cat <<-HERE
					Disk /dev/sdu5: 10 GB, 10000000000 bytes, 71943040 sectors
					Units: sectors of 1 * 1024 = 1024 bytes
					Sector size (logical/physical): 1024 bytes / 512 bytes
					I/O size (minimum/optimal): 1024 bytes / 512 bytes
					HERE
					;;
				/dev/sdu)
					cat <<-HERE
					Disk /dev/sdu: 124.1 GiB, 124100000000 bytes, 504212490 sectors
					Disk model: POSHIBA 0RC4741L
					Units: sectors of 1 * 1024 = 1024 bytes
					Sector size (logical/physical): 1024 bytes / 1024 bytes
					I/O size (minimum/optimal): 1024 bytes / 512 bytes
					Disklabel type: gpt
					Disk identifier: 42398402-429B-FF42-CA2B-42910C493206

					Device         Start       End   Sectors  Size Type
					/dev/sdu1       4292  43215152  71943040   20G Linux filesystem
					/dev/sdu2   45354358  63214213  27777216    4G Linux filesystem
					/dev/sdu3   64364342  52142152    854656  100M Unreadable rubbish
					/dev/sdu4   52312410 443214214 325728256   90G Linux filesystem
					/dev/sdu5  443463254 532523323  50164431   10G Linux filesystem
					HERE
					;;
			esac
			;;
		/dev/sdu)
			cat;
			;;
	esac
}

ws() {
	case $1 in
		--config)
			case "$(echo $@ | awk '{ print $NF }')" in
				device)
					echo "/dev/sdu";
					;;
				disk/size)
					echo "$PARTITIONS";
					;;
			esac
			;;
		*)
			command ws "$@";
			;;
	esac
}
