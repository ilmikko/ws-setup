assert_enough_space() {
	if [ "$1" -lt 0 ]; then
		log "Not enough space on $device!";
		log "The device would need $1 bytes of space more to accommodate the partitions.";
		return 1;
	else
		log "$(echo $1 | humanize_bytes) remaining.";
	fi
}

assert_relative_add_up_to_100() {
	# Second, we want to make sure percentages add up to 100%, and see how many there are.
	perc_count=0;
	perc_sum=0;
	count=0;

	for partition in $@; do
		count=$((count + 1));
		size=$(echo "$partition" | awk -F: '{ print $2 }');

		is_relative "$size" || continue;

		perc_sum=$(echo "$perc_sum" "$size" | awk '{ printf("%i", $1+$2) }');
		perc_count=$((perc_count + 1));
	done

	if [ "$perc_count" -gt 0 ] && [ "$perc_sum" != 100 ]; then
		log "Percentages do not add up to 100%!";
		log "Currently they add up to $perc_sum%.";
		exit 1;
	fi
}

fdisk_check_label() {
	label=$(fdisk -l $device | awk '/label type/ { print $NF }');
	case $1 in
		[Gg][Pp][Tt])
			[ "$label" = "gpt" ] || exit 1;
			;;
		[Dd][Oo][Ss]|[Mm][Bb][Rr])
			[ "$label" = "dos" ] || exit 1;
			;;
		[Ss][Uu][Nn])
			[ "$label" = "sun" ] || exit 1;
			;;
		[Ss][Gg][Ii]|[Ii][Rr][Ii][Xx])
			[ "$label" = "sgi" ] || exit 1;
			;;
		*)
			log "Unknown partition table label: $1";
			exit 1;
			;;
	esac

	log "Partition label matches. ($label)";
}

fdisk_create_partition() {
	(
	echo n # new partition
	echo # default number
	echo # default start sector
	if [ -z "$1" ]; then
		echo # Use rest of space
	else
		echo +$(awk 'BEGIN { printf("%i", '$1'/1024) }')K # Use this much space
	fi
	) >> $fdisk_file;
}

fdisk_file=$(mktemp);

fdisk_set_label() {
	case $1 in
		[Gg][Pp][Tt])
			key=g;
			;;
		[Dd][Oo][Ss]|[Mm][Bb][Rr])
			key=o;
			;;
		[Ss][Uu][Nn])
			key=s;
			;;
		[Ss][Gg][Ii]|[Ii][Rr][Ii][Xx])
			key=G;
			;;
		*)
			log "Unknown partition table label: $1";
			exit 1;
			;;
	esac
	echo $key > $fdisk_file;
}

fdisk_write() {
	echo w >> $fdisk_file;

	cat $fdisk_file | fdisk $device;

	rm $fdisk_file;
}

get_available_bytes() {
	fdisk -l "$1" | awk '{ print $5; exit }';
}

humanize_bytes() {
	awk "$(cat <<-AWK
		function abs(v) { return v < 0 ? -v : v }
		{
			bytes = \$1;
			order = log(bytes)/log(2);

			if (order < 10) {
				bytes = bytes;
				unit = "B";
			} else if (order < 20) {
				bytes = (bytes/1024);
				unit = "KiB";
			} else if (order < 30) {
				bytes = (bytes/1048576);
				unit = "MiB";
			} else if (order < 40) {
				bytes = (bytes/1073741824);
				unit = "GiB";
			} else {
				bytes = (bytes/1099511627776);
				unit = "TiB";
			}

			printf("%.1f"unit, bytes);
		}
	AWK
	)"
}

is_relative() {
	[ "$(echo $1 | awk '{ print substr($1,length($1),1) }')" = "%" ];
}

partition_absolute() {
	log "$(echo $remaining_bytes | humanize_bytes) available on $device.";

	# First, we want to see how much space the absolute values take.
	for partition in $@; do
		size=$(echo "$partition" | awk -F: '{ print $2 }');

		is_relative "$size" && continue;

		# Convert size to bytes.
		size=$(echo "$size" | to_bytes);

		remaining_bytes=$(awk 'BEGIN { printf("%i", '"$remaining_bytes"'-'"$size"') }');
	done
}

partition_relative_check() {
	# Here we do a comparison for sizes of partitions.
	# We can get rough human-readable values from lsblk.
	for partition in $@; do
		size=$(echo "$partition" | awk -F: '{ print $2 }');

		if is_relative "$size"; then
			# Calculate percentage of the remaining bytes.
			perc="$size";
			size=$(echo "$perc" "$remaining_bytes" | awk '{ printf("%i", $2*($1/100)) }');

			[ "$size" -gt 0 ] || exit 1;
		else
			size=$(echo "$size" | to_bytes);
		fi

		count=$((count - 1));
		number=$((partition_count - count));

		block="$(ws device_name "device=$device" "partition=$number")" || exit 1;

		actual_size=$(get_available_bytes "$block");
		printf "%s" "$block size $size (calc) vs $actual_size (real).";

		difference=$(awk 'BEGIN { a='"$actual_size"'; b='"$size"'; if (a < b) { c = a; a = b; b = c }; printf("%i", (a - b)/1024); }');

		# As we omit the last one's size, if all the other partitions have
		# matched, unless our physical disk size has changed there is no way the
		# last partition is a different size.

		log " $difference""KB diff";
		# If >1MB difference, we need to repartition.
		[ "$count" = 0 ] || [ "$difference" -lt 1024 ] || exit 1;
	done
}

partition_relative() {
	# Finally, we want to allocate the relative size that is left, and write everything to the disk.
	for partition in $@; do
		size=$(echo "$partition" | awk -F: '{ print $2 }');

		if is_relative "$size"; then
			# Calculate percentage of the remaining bytes.
			perc="$size";
			size=$(echo "$perc" "$remaining_bytes" | awk '{ printf("%i", $2*($1/100)) }');

			if [ ! "$size" -gt 0 ]; then
				log "Not enough space on $device for relative partitions!";
				log "Taking $perc of $remaining_bytes bytes yields $size bytes.";
				exit 1;
			fi
		else
			size=$(echo "$size" | to_bytes);
			fi

			count=$((count - 1));

		# Due to rounding errors, we want to omit the size for our last partition.
		if [ "$count" = 0 ]; then
			fdisk_create_partition;
		else
			fdisk_create_partition $size;
		fi
	done
}

to_bytes() {
	awk "$(cat <<-AWK
	{
		type = toupper(\$1);
		bytes = \$1;

		sub("^[0-9.]*", "", type);
		sub("[^0-9.]*$", "", bytes);

		switch(type) {
		case "TIB":case "T":
			bytes *= 1099511627776;
			break;
		case "GIB":case "G":
			bytes *= 1073741824;
			break;
		case "MIB":case "M":
			bytes *= 1048576;
			break;
		case "KIB":case "K":
			bytes *= 1024;
			break;
		case "TB":
			bytes *= 1e12;
			break;
		case "GB":
			bytes *= 1e9;
			break;
		case "MB":
			bytes *= 1e6;
			break;
		case "KB":
			bytes *= 1e3;
			break;
		case "B":
			break;
		}
		print(bytes);
	}
	AWK
	)"
}
