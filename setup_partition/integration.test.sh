. "setup_partition/lib.mock.sh" || exit 222;

ln -sf "setup_partition/lib.sh";

test_check_pass() {
	# 1. Already partitioned disk that should pass the check.

	PARTITIONS=$(cat <<HERE
1:20GB
2:4GB
3:100MB
4:90%
5:10%
HERE
);

X=$(. "setup_partition/CHECK") || exit 1;
}

test_check_fail() {
	# 2. Already partitioned disk that should fail the check.

	PARTITIONS=$(cat <<HERE
1:20GB
2:4GB
3:100MB
4:50%
5:50%
HERE
);

X=$(. "setup_partition/CHECK") && exit 1;
true;
}

test_run_fdisk() {
	# 3. Correct formatting of fdisk parameters.

	PARTITIONS=$(cat <<HERE
1:20GB
2:4GB
3:100MB
4:50%
5:50%
HERE
);

! ! assert_equals "$(. "setup_partition/RUN")" "$(cat <<HERE
115.6GiB available on /dev/sdu.
93.1GiB remaining.
g
n


+19531250K
n


+3906250K
n


+97656K
n


+48828125K
n



w
HERE
)" || exit 1;
}
