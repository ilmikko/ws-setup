. "setup_partition/lib.mock.sh" || exit 222;
. "setup_partition/lib.sh" || exit 222;

test_assert_enough_space() {
	assert_success "assert_enough_space 200";
	assert_success "assert_enough_space 0";
	assert_fail "assert_enough_space -200";
}

test_fdisk_check_label() {
	assert_equals "$(fdisk_check_label "gpt" 2>&1)" "Partition label matches. (gpt)";
	assert_equals "$(fdisk_check_label "GPT" 2>&1)" "Partition label matches. (gpt)";
	assert_equals "$(fdisk_check_label "dos" 2>&1)" "";
	assert_equals "$(fdisk_check_label "rarara" 2>&1)" "Unknown partition table label: rarara";
}

test_get_available_bytes() {
	assert_equals "$(get_available_bytes "/dev/sdu")" "124100000000";
	assert_equals "$(get_available_bytes "/dev/sdu4")" "90000000000";
}

test_humanize_bytes() {
	assert_equals "$(echo 0 | humanize_bytes)" "0.0B";
	assert_equals "$(echo 1 | humanize_bytes)" "1.0B";
	assert_equals "$(echo 10 | humanize_bytes)" "10.0B";
	assert_equals "$(echo 1000 | humanize_bytes)" "1000.0B";
	assert_equals "$(echo 1024 | humanize_bytes)" "1.0KiB";
	assert_equals "$(echo 2048 | humanize_bytes)" "2.0KiB";
	assert_equals "$(echo 4095 | humanize_bytes)" "4.0KiB";
	assert_equals "$(echo 10000 | humanize_bytes)" "9.8KiB";
	assert_equals "$(echo 100000 | humanize_bytes)" "97.7KiB";
	assert_equals "$(echo 1000000 | humanize_bytes)" "976.6KiB";
	assert_equals "$(echo 1048575 | humanize_bytes)" "1024.0KiB";
	assert_equals "$(echo 1048576 | humanize_bytes)" "1.0MiB";
	assert_equals "$(echo 10000000 | humanize_bytes)" "9.5MiB";
	assert_equals "$(echo 100000000 | humanize_bytes)" "95.4MiB";
	assert_equals "$(echo 1000000000 | humanize_bytes)" "953.7MiB";
	assert_equals "$(echo 1073741824 | humanize_bytes)" "1.0GiB";
	assert_equals "$(echo 10000000000 | humanize_bytes)" "9.3GiB";
	assert_equals "$(echo 100000000000 | humanize_bytes)" "93.1GiB";
	assert_equals "$(echo 1000000000000 | humanize_bytes)" "931.3GiB";
	assert_equals "$(echo 1099511627776 | humanize_bytes)" "1.0TiB";
	assert_equals "$(echo 2199023255552 | humanize_bytes)" "2.0TiB";
}

test_to_bytes() {
	assert_equals "$(echo "2RIB" | to_bytes)" "2";
	assert_equals "$(echo "TIB" | to_bytes)" "0";
	assert_equals "$(echo "0" | to_bytes)" "0";
	assert_equals "$(echo "0GiB" | to_bytes)" "0";
	assert_equals "$(echo "1" | to_bytes)" "1";
	assert_equals "$(echo "1000" | to_bytes)" "1000";
	assert_equals "$(echo "1K" | to_bytes)" "1024";
	assert_equals "$(echo "1KiB" | to_bytes)" "1024";
	assert_equals "$(echo "1KB" | to_bytes)" "1000";
	assert_equals "$(echo "2K" | to_bytes)" "2048";
	assert_equals "$(echo "1M" | to_bytes)" "1048576";
	assert_equals "$(echo "1MiB" | to_bytes)" "1048576";
	assert_equals "$(echo "1MB" | to_bytes)" "1000000";
	assert_equals "$(echo "1G" | to_bytes)" "1073741824";
	assert_equals "$(echo "1GiB" | to_bytes)" "1073741824";
	assert_equals "$(echo "1GB" | to_bytes)" "1000000000";
	assert_equals "$(echo "1T" | to_bytes)" "1099511627776";
	assert_equals "$(echo "1TiB" | to_bytes)" "1099511627776";
	assert_equals "$(echo "1TB" | to_bytes)" "1000000000000";
}

test_is_relative() {
	assert_success "is_relative 100%";
	assert_success "is_relative 0%";
	assert_success "is_relative -50%";
	assert_fail "is_relative 10";
	assert_fail "is_relative 50KiB";
	assert_fail "is_relative relative";
}

test_partition_absolute() {
	remaining_bytes=1000000000; # 1GiB
	partition_absolute "1:1000" "2:1000000" "3:50%" "4:52515523" "5:15%" "6:35%";
	assert_equals "$remaining_bytes" "946483477";
}

test_partition_relative() {
	remaining_bytes=1000000000; # 1GiB
	fdisk_file=$(mktemp);
	partition_relative "1:20GB" "2:8GiB" "3:450MiB" "4:159GiB" "5:24GiB";
	! ! assert_equals "$(cat "$fdisk_file")" "$(cat <<-HERE
	n


	+19531250K
	n


	+8388608K
	n


	+460800K
	n


	+166723584K
	n


	+25165824K
	HERE
	)" || exit 1;

	fdisk_file=$(mktemp);
	partition_relative "1:48GiB" "2:8GiB" "3:50%" "4:159GiB" "5:24GiB";
	! ! assert_equals "$(cat "$fdisk_file")" "$(cat <<-HERE
	n


	+50331648K
	n


	+8388608K
	n


	+488281K
	n


	+166723584K
	n


	+25165824K
	HERE
	)" || exit 1;

	fdisk_file=$(mktemp);
	partition_relative "1:20%" "2:30%" "3:50%" "4:159GiB" "5:24GiB";
	! ! assert_equals "$(cat "$fdisk_file")" "$(cat <<-HERE
	n


	+195312K
	n


	+292968K
	n


	+488281K
	n


	+166723584K
	n


	+25165824K
	HERE
	)" || exit 1;
}

test_partition_relative_check() {
	remaining_bytes=100000000000; # 100GB
	assert_success "partition_relative_check 1:20GB 2:4GB 3:100MB 4:90GB 5:10GB";
	assert_success "partition_relative_check 1:20GB 2:4GB 3:100.5MB 4:90GB 5:10GB";
	assert_success "partition_relative_check 1:20GB 2:4GB 3:100MB 4:90% 5:10%";
	assert_fail "partition_relative_check 1:21GB 2:4GB 3:100MB 4:90% 5:10%";
	assert_fail "partition_relative_check 1:20GB 2:4GB 3:100MiB 4:90% 5:10%";
	assert_fail "partition_relative_check 1:20GB 2:4% 3:100MB 4:90% 5:6%";
}

test_assert_relative_add_up_to_100() {
	assert_success "assert_relative_add_up_to_100 1:1000 2:1000000 3:50% 4:52515523 5:15% 6:35%";
	assert_success "assert_relative_add_up_to_100 1:50% 2:50%";
	assert_success "assert_relative_add_up_to_100 1:1000 2:1000000";
	assert_fail "assert_relative_add_up_to_100 1:50% 2:1000000";
	assert_fail "assert_relative_add_up_to_100 1:50% 2:25% 3:26%";
}
